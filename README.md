# ApplaudoWeek5

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.5. Burger builder with predefined ingredients that calculates automatically the price (You can change the default currency too). You can save your own burgers and then watch or delete them again in the burger history. Deployment is available on https://applaudo-week-5.vercel.app/home/0

<img src="src/assets/img/1.png">
<img src="src/assets/img/2.png">
<img src="src/assets/img/3.png">

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
