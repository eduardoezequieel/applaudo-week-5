import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BurgerCreatorComponent } from './pages/burger-creator/burger-creator.component';
import { BurgerHistoryComponent } from './pages/burger-history/burger-history.component';

const routes: Routes = [
  { path: '', redirectTo: '/home/0', pathMatch: 'full' },
  { path: 'home/:id', component: BurgerCreatorComponent },
  { path: 'history', component: BurgerHistoryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
