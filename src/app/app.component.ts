import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  url?: string;
  title?: string;
  icon?: string;

  logUrl(component: any) {
    if (component.router.url != '/history') {
      this.url = '/home';
    } else {
      this.url = component.router.url;
    }

    if (this.url == '/home') {
      this.title = 'Burger Creator';
      this.icon = 'fa-solid fa-burger';
    } else {
      this.title = 'Burger History';
      this.icon = 'fa-solid fa-clock';
    }
  }
}
