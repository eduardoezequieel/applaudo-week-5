import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonComponent } from './components/button/button.component';
import { BurgerComponent } from './components/burger/burger.component';
import { AddButtonsComponent } from './components/add-buttons/add-buttons.component';
import { RemoveButtonsComponent } from './components/remove-buttons/remove-buttons.component';
import { BurgerCreatorComponent } from './pages/burger-creator/burger-creator.component';
import { BurgerHistoryComponent } from './pages/burger-history/burger-history.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ContainerComponent } from './pages/container/container.component';
import { HeaderComponent } from './components/header/header.component';
import { TableComponent } from './components/table/table.component';
import { CurrencyConverterPipe } from './pipes/currency-converter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    BurgerComponent,
    AddButtonsComponent,
    RemoveButtonsComponent,
    BurgerCreatorComponent,
    BurgerHistoryComponent,
    NavbarComponent,
    ContainerComponent,
    HeaderComponent,
    TableComponent,
    CurrencyConverterPipe,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
