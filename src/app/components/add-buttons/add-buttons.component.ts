import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-buttons',
  templateUrl: './add-buttons.component.html',
  styleUrls: ['./add-buttons.component.scss'],
})
export class AddButtonsComponent {
  @Output() ingredient = new EventEmitter();

  emitIngredient(ingredient: string) {
    this.ingredient.emit(ingredient);
  }
}
