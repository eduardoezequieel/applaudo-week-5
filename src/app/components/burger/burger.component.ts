import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-burger',
  templateUrl: './burger.component.html',
  styleUrls: ['./burger.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BurgerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
