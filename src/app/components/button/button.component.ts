import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() icon?: string;
  @Input() class?: string;
  @Input() text?: string;
  @Input() value?: string;
  @Input() isDisabled?: boolean;
  @Output() event = new EventEmitter();

  logEvent(e: any): void {
    this.event.emit(e.target.getAttribute('value'));
  }
}
