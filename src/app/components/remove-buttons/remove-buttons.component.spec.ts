import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveButtonsComponent } from './remove-buttons.component';

describe('RemoveButtonsComponent', () => {
  let component: RemoveButtonsComponent;
  let fixture: ComponentFixture<RemoveButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveButtonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
