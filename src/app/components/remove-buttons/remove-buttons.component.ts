import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-remove-buttons',
  templateUrl: './remove-buttons.component.html',
  styleUrls: ['./remove-buttons.component.scss'],
})
export class RemoveButtonsComponent {
  @Output() ingredient = new EventEmitter();
  @Input() isDisabled: boolean = true;

  emitIngredient(ingredient: string) {
    this.ingredient.emit(ingredient);
  }
}
