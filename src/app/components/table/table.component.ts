import { TableService } from './../../services/table.service';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @Input() currencyCode: string = 'USD';

  constructor(public tableService: TableService) {}
}
