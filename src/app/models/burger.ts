import { StorageService } from '../services/storage.service';
import { Ingredient } from './ingredient';

export class Burger {
  public creationDate: string;

  constructor(public id: number, public ingredients: Ingredient[], public isBurgerSaved: boolean) {
    this.creationDate = this.generateDate();
  }

  private generateDate(): string {
    const date = new Date();
    return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
  }
}
