export class Ingredient {
  constructor(public ingredient: string, public removable: boolean) {}
}
