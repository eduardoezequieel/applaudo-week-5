import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BurgerCreatorComponent } from './burger-creator.component';

describe('BurgerCreatorComponent', () => {
  let component: BurgerCreatorComponent;
  let fixture: ComponentFixture<BurgerCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BurgerCreatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BurgerCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
