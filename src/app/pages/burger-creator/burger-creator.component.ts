import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BurgersService } from 'src/app/services/burgers.service';

@Component({
  selector: 'app-burger-creator',
  templateUrl: './burger-creator.component.html',
  styleUrls: ['./burger-creator.component.scss'],
})
export class BurgerCreatorComponent implements OnInit {
  @Output() url = new EventEmitter();
  saveDisabled: boolean = true;
  currencyCode: string = 'USD';
  burgerFromHistory: boolean = false;
  id?: number;

  constructor(
    public burgersService: BurgersService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.burgersService.initBurger(this.id);
    this.isSaveDisabled();

    if (this.id != 0) {
      this.burgerFromHistory = true;
    }
  }

  reloadComponent() {
    let currentUrl = '/home/0';
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  isSaveDisabled() {
    console.log(this.burgersService.ingredients);
    this.saveDisabled = this.burgersService.ingredients.length > 0 ? false : true;
  }

  changeCurrency(e: any) {
    this.currencyCode = e.target.value;
  }

  addIngredient(className: string): void {
    this.burgersService.addIngredient(className);
    this.isSaveDisabled();
  }

  removeIngredient(className: string): void {
    this.burgersService.removeIngredient(className);
    setTimeout(() => {
      this.isSaveDisabled();
    }, 200);
  }
}
