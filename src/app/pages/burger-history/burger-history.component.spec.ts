import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BurgerHistoryComponent } from './burger-history.component';

describe('BurgerHistoryComponent', () => {
  let component: BurgerHistoryComponent;
  let fixture: ComponentFixture<BurgerHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BurgerHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BurgerHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
