import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Burger } from 'src/app/models/burger';
import { BurgersService } from 'src/app/services/burgers.service';

@Component({
  selector: 'app-burger-history',
  templateUrl: './burger-history.component.html',
  styleUrls: ['./burger-history.component.scss'],
})
export class BurgerHistoryComponent implements OnInit {
  burgers: Burger[] = [];
  constructor(public burgersService: BurgersService, private router: Router) {}

  ngOnInit(): void {
    this.burgers = this.burgersService.getBurgers();
  }

  deleteBurger(id: number) {
    this.burgersService.deleteBurger(id);
    this.burgers = this.burgersService.getBurgers();
  }
}
