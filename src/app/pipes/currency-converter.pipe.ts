import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyConverter',
})
export class CurrencyConverterPipe implements PipeTransform {
  transform(value: number, ...args: string[]): unknown {
    const [currencyCode] = args;

    switch (currencyCode) {
      case 'USD':
        return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(value);
      case 'EUR':
        value = value * 0.95;
        return new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'EUR' }).format(value);
      case 'MXN':
        value = value * 19.86;
        return new Intl.NumberFormat('es-MX', { style: 'currency', currency: 'MXN' }).format(value);
      case 'AUD':
        value = value * 1.42;
        return new Intl.NumberFormat('en-AU', { style: 'currency', currency: 'AUD' }).format(value);
      case 'JPY':
        value = value * 127.94;
        return new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'JPY' }).format(value);
      default:
        return 0;
    }
  }
}
