import { TableService } from './table.service';
import { Injectable } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { Ingredient } from '../models/ingredient';
import { Burger } from '../models/burger';

@Injectable({
  providedIn: 'root',
})
export class BurgersService {
  burgers: Burger[] = [];
  ingredients: Ingredient[] = [];

  isThereAnyBurger: boolean = false;
  ingredientsGettingCleaned: boolean = false;
  burgerId?: number;
  burgerPosition?: number;
  total: number = 0;

  constructor(private storageService: StorageService, private tableService: TableService) {}

  resetBurger(): void {
    this.ingredients = [];
    this.isThereAnyBurger = false;
    this.burgerId = undefined;
    this.burgerPosition = undefined;
    this.total = this.tableService.loadTable(this.ingredients);
  }

  initBurger(id: number): void {
    this.resetBurger();

    this.storageService.getData();

    this.burgers = this.storageService.burgers;

    let burger;

    if (id == 0) {
      for (let i = 0; i < this.burgers.length; i++) {
        if (this.burgers[i].isBurgerSaved == false) {
          burger = this.burgers[i];
          this.isThereAnyBurger = true;
          break;
        }
      }

      if (this.isThereAnyBurger) {
        this.burgerId = burger?.id;
        this.ingredients = burger?.ingredients!;
        this.burgerPosition = this.storageService.find(burger?.id!);
        this.total = this.tableService.loadTable(this.ingredients);
      }
    } else {
      this.ingredients = this.burgers[this.storageService.find(id)].ingredients;
      this.total = this.tableService.loadTable(this.ingredients);
    }
  }

  createBurger(): void {
    let burger = new Burger(this.storageService.generateId(), this.ingredients, false);

    this.storageService.add(burger);

    this.burgerId = burger.id;
    this.burgerPosition = this.storageService.find(burger.id);
    console.log(this.storageService.find(burger.id));
    this.isThereAnyBurger = true;
  }

  saveBurger(): void {
    if (confirm('Are you sure that you wanna save this burger?')) {
      this.burgers[this.burgerPosition!].isBurgerSaved = true;
      this.storageService.update(this.burgerPosition!, this.ingredients);

      this.ingredientsGettingCleaned = true;

      setTimeout(() => {
        this.ingredientsGettingCleaned = false;
        this.initBurger(0);
      }, 200);
    }
  }

  deleteBurger(id: number) {
    if (confirm('Are you sure that you wanna delete this burger?')) {
      this.storageService.delete(id);
    }
  }

  getBurgers(): Burger[] {
    let burgers: Burger[] = [];
    this.storageService.getData();

    this.storageService.burgers.forEach((burger) => {
      if (burger.isBurgerSaved) {
        burgers.push(burger);
      }
    });

    return burgers;
  }

  addIngredient(className: string): void {
    if (this.isThereAnyBurger) {
      this.ingredients.push(new Ingredient(className, false));
      this.storageService.update(this.burgerPosition!, this.ingredients);
      this.total = this.tableService.loadTable(this.ingredients);
    } else {
      this.ingredients.push(new Ingredient(className, false));
      this.createBurger();
      this.total = this.tableService.loadTable(this.ingredients);
    }
  }

  removeIngredient(className: string): void {
    let index: number = 0;
    let foundSomething: boolean = false;

    for (let i = 0; i < this.ingredients.length; i++) {
      if (this.ingredients[i].ingredient == className) {
        index = i;
        foundSomething = true;
        break;
      }
    }

    if (foundSomething) {
      this.ingredients[index].removable = true;

      setTimeout(() => {
        this.ingredients.splice(index, 1);
        this.storageService.update(this.burgerPosition!, this.ingredients);
        this.total = this.tableService.loadTable(this.ingredients);
      }, 200);
    } else {
      alert(`You didn't add any ${className} before!`);
    }
  }
}
