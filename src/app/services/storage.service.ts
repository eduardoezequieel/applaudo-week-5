import { Ingredient } from 'src/app/models/ingredient';
import { Injectable } from '@angular/core';
import { Burger } from '../models/burger';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  public burgers: Burger[] = [];

  add(burger: Burger): void {
    this.burgers.push(burger);
    this.save();
  }

  update(id: number, ingredients: Ingredient[]) {
    this.burgers[id].ingredients = ingredients;
    this.save();
  }

  delete(id: number): void {
    let index: number = this.find(id);

    this.burgers.splice(index, 1);
    this.save();
  }

  find(id: number) {
    let index: number = 0;

    for (let i = 0; i < this.burgers.length; i++) {
      if (this.burgers[i].id == id) {
        index = i;
        break;
      }
    }

    return index;
  }

  generateId(): number {
    let length = this.burgers.length;
    let generatedId = 1;

    if (length > 0) {
      generatedId = this.burgers[length - 1].id + 1;
    }

    return generatedId;
  }

  save(): void {
    localStorage.setItem('burgers', JSON.stringify(this.burgers));
  }

  getData(): void {
    let data = JSON.parse(localStorage.getItem('burgers')!);
    this.burgers = data != null ? data : [];
  }
}
