import { Injectable } from '@angular/core';
import { Ingredient } from 'src/app/models/ingredient';

interface IngredientsTable {
  name: string;
  units: number;
  price: number;
}

@Injectable({
  providedIn: 'root',
})
export class TableService {
  uniqueIngredients = new Set<string>();
  table: IngredientsTable[] = [];

  loadTable(ingredients: Ingredient[]): number {
    this.getUniqueIngredients(ingredients);
    this.initTable(ingredients);
    return this.calcTotal();
  }

  getUniqueIngredients(ingredients: Ingredient[]): void {
    ingredients.forEach((ingredient) => {
      this.uniqueIngredients.add(ingredient.ingredient);
    });
  }

  initTable(ingredients: Ingredient[]): void {
    this.table = [];
    this.table.push({ name: 'Bread', units: 2, price: 1 });

    this.uniqueIngredients.forEach((ingredient) => {
      let units = this.calcUnits(ingredients, ingredient);
      let price = this.calcPrice(ingredient, units);

      if (units > 0) {
        this.table.push({
          name: ingredient,
          units: units,
          price: price,
        });
      }
    });
  }

  calcUnits(ingredients: Ingredient[], ingredient: string): number {
    let units = 0;
    ingredients.forEach((row) => {
      if (row.ingredient == ingredient) {
        units++;
      }
    });
    return units;
  }

  calcPrice(ingredient: string, units: number): number {
    let price: number = 0;

    switch (ingredient) {
      case 'meat':
        price = 1.5 * units;
        break;
      case 'cheese':
        price = 0.5 * units;
        break;
      case 'salad':
        price = 0.25 * units;
        break;
      case 'cucumber':
        price = 0.75 * units;
        break;
      case 'pepper':
        price = 1 * units;
        break;
      default:
        price = 0;
        break;
    }

    return price;
  }

  calcTotal(): number {
    let total: number = 0;

    this.table.forEach((row) => {
      total += row.price;
    });

    return total;
  }
}
